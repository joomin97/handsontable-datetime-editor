# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
   
1. This is datetimepicker editor for handsontable.
2. datetimepicker use jquery date/time picker.

   * jquery(http://jquery.com/download/)
   * jquery-ui(https://jqueryui.com/download/)
   * handsontable(http://handsontable.com/)

### How do I get set up? ###

```
<div id="exampleHandsonTable"></div>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
	var hot;
	var container = document.getElementById("exampleHandsonTable");

	hot = new Handsontable(container, {
		rowHeaders: true,
		manualColumnResize: true,
		colHeaders: ['datetime', 'numeric'],
		columns: [
		          {
		        	  editor: 'datetime',
		        	  dateFormat: 'yy/mm/dd',
		        	  timeFormat: 'HH:mm:ss'
		          },
		          {
		        	  type: 'numeric'
		          }
		          ],
	});
});

</script>
```

![sample.png](https://bitbucket.org/repo/b4o8AX/images/3753349520-sample.png)